package com.nagp.hotel.controller;


import com.nagp.hotel.model.HotelBookingRequestModel;
import com.nagp.hotel.model.HotelBookingResponseModel;
import com.nagp.hotel.model.PaymentModel;
import com.nagp.hotel.service.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.nagp.hotel.constants.HotelConstants.SEARCH_HOTEL;

@RestController
@Slf4j
@RequiredArgsConstructor
public class HotelController {
    @Value("${server.port}")
    private int port;
    private final HotelService hotelService;
    @PostMapping(value = SEARCH_HOTEL)
    public ResponseEntity<?> fetchHotels(@RequestBody HotelBookingRequestModel bookingRequestModel) {
        log.info("Received request for fetching the hotel details");
        System.out.println("Working from port " + port + " of hotel service");
        List<HotelBookingResponseModel> responseModels = hotelService.fetchHotelDetails(bookingRequestModel);
        return ResponseEntity.ok().body(responseModels);
    }
    @PostMapping("/bookRoom")
    public String bookRooms(@RequestBody PaymentModel paymentModel){
        System.out.println("Working from port " + port + " of hotel service");
        return hotelService.bookRooms(paymentModel);
    }
}
