package com.nagp.hotel.service;

import com.nagp.hotel.model.HotelBookingRequestModel;
import com.nagp.hotel.model.HotelBookingResponseModel;
import com.nagp.hotel.model.PaymentModel;
import com.nagp.hotel.repository.HotelRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class HotelService {
    @Autowired
    private HotelRepo hotelRepo;
    public List<HotelBookingResponseModel> fetchHotelDetails(final HotelBookingRequestModel bookingRequestModel) {
        return hotelRepo.findAll();
    }
    public String bookRooms(PaymentModel paymentModel){
        HotelBookingResponseModel data = hotelRepo.findByHotelCode(paymentModel.getCode());
        if(data.getAvailableRooms()<1){
          return "Booking failed";
        }int rooms = data.getAvailableRooms()-1;
        data.setAvailableRooms(rooms);
        hotelRepo.save(data);
        return "Booking has been confirmed ";
    }
}
