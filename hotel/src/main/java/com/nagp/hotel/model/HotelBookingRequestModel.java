package com.nagp.hotel.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelBookingRequestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    private String city;
    private String hotelCode;
    private String checkInTime;
    private String checkOutTime;
    private int noOfRooms;
    private int noOfGuests;
    private int price;
    private String type;
}
