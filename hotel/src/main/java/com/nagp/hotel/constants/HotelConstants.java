package com.nagp.hotel.constants;

public class HotelConstants {
    private HotelConstants() {
        throw new IllegalArgumentException("Constants Class Instance Can't be created");}
    public static final String CREATE_HOTEL_BOOKING = "/bookHotel";
    public static final String SEARCH_HOTEL = "/hotel/search";
}
