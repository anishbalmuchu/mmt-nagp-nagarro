package com.nagp.hotel.constants;

import com.nagp.hotel.model.HotelBookingResponseModel;
import com.nagp.hotel.repository.HotelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Configuration
@Component
public class ConfigData {
@Autowired
private HotelRepo hotelRepo;
    @Bean
    public void addData (){
        List<HotelBookingResponseModel> flights = Arrays.asList(new HotelBookingResponseModel(null,"MUM001", "Taj Mahal Palace", "Apollo Bunder, Mumbai", 20000,1),
                new HotelBookingResponseModel(null,"MUM002", "The Oberoi Mumbai", "Nariman Point, Mumbai", 18000,1),
                new HotelBookingResponseModel(null,"MUM003", "Grand Hyatt Mumbai Hotel and Residences", "Off Western Express Highway, Santacruz East, Mumbai", 12000,1));
        flights.forEach(flight -> hotelRepo.save(flight));
    }
}
