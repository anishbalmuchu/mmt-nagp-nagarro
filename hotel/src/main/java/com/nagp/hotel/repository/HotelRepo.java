package com.nagp.hotel.repository;

import com.nagp.hotel.model.HotelBookingResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepo extends JpaRepository<HotelBookingResponseModel,Long> {
   HotelBookingResponseModel findByHotelCode(String hotelCode);
}
