package com.nagp.flight.repository;


import com.nagp.flight.model.FlightBookingResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepo extends JpaRepository<FlightBookingResponseModel,Long> {
    FlightBookingResponseModel findByFlightCode(String flightCode);
}
