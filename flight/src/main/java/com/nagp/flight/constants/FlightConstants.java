package com.nagp.flight.constants;

public class FlightConstants {
    private FlightConstants() {
        throw new IllegalArgumentException("Constants Class Instance Can't be created");}
    public static final String CREATE_FLIGHT_BOOKING = "/bookFlights";
    public static final String SEARCH_FLIGHT = "/flight/search";
}
