package com.nagp.flight.constants;

import com.nagp.flight.model.FlightBookingResponseModel;
import com.nagp.flight.repository.FlightRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Configuration
@Component
public class ConfigData {
   @Autowired
    private FlightRepo flightRepo;
   @Bean
    public void addData (){
            List<FlightBookingResponseModel> flights = Arrays.asList(new FlightBookingResponseModel(null,"AA123", "American Airlines", "2023-04-01T08:00:00.000Z","2023-04-01T10:00:00.000Z", 20000,1),
                    new FlightBookingResponseModel(null,"UA456", "United Airlines", "2023-04-01T09:00:00.000Z","2023-04-01T11:00:00.000Z", 18000,1),
                    new FlightBookingResponseModel(null,"DL789", "Delta Airlines", "2023-04-01T06:00:00.000Z","2023-04-01T08:00:00.000Z", 12000,1));
            flights.forEach(flight -> flightRepo.save(flight));
    }
}
