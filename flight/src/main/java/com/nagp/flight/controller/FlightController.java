package com.nagp.flight.controller;


import com.nagp.flight.model.FlightBookingRequestModel;
import com.nagp.flight.model.FlightBookingResponseModel;
import com.nagp.flight.model.PaymentModel;
import com.nagp.flight.service.FlightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.nagp.flight.constants.FlightConstants.CREATE_FLIGHT_BOOKING;
import static com.nagp.flight.constants.FlightConstants.SEARCH_FLIGHT;

@RestController
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(exposedHeaders = {"Access-Control-Allow-Origin","Access-Control-Allow-Credentials"})
public class FlightController {
    private final FlightService service;
    @PostMapping(value = SEARCH_FLIGHT)
    public ResponseEntity<?> fetchFlights(@RequestBody FlightBookingRequestModel bookingRequestModel) {
        List<FlightBookingResponseModel> responseModels = service.fetchFlightDetails(bookingRequestModel);
        return ResponseEntity.ok().body(responseModels);
    }
    @PostMapping(value = CREATE_FLIGHT_BOOKING)
    public String bookFlight(@RequestBody PaymentModel paymentModel) {
        return service.bookFlight(paymentModel);
    }
}
