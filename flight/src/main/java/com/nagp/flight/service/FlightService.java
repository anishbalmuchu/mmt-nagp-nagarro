package com.nagp.flight.service;

import com.nagp.flight.model.FlightBookingRequestModel;
import com.nagp.flight.model.FlightBookingResponseModel;
import com.nagp.flight.model.PaymentModel;
import com.nagp.flight.repository.FlightRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class FlightService {
    @Autowired
    private FlightRepo flightRepo;
    public List<FlightBookingResponseModel> fetchFlightDetails(final FlightBookingRequestModel bookingRequestModel) {
        return flightRepo.findAll();
    }
    public String bookFlight(PaymentModel paymentModel){
        FlightBookingResponseModel departureFlightData = flightRepo.findByFlightCode(paymentModel.getCode());
        FlightBookingResponseModel returnFlightData = flightRepo.findByFlightCode(paymentModel.getReturnCode());

        // Departure flight seats check
        if(departureFlightData.getAvailableSeats() < paymentModel.getNoOfPassengers() ){
            log.info("Booking failed. No seats left in departing flight, your payment refund has been initiated.");
            return "Booking failed. No seats left in departing flight, your payment refund has been initiated.";
        }
        // Return flight seats check
        if(paymentModel.getIsRoundTrip().equalsIgnoreCase("YES")) {
            if(returnFlightData == null) {
                log.info("Booking failed. Please check correct return flight code.");
                return "Booking failed. Please check correct return flight code.";
            }
            if(returnFlightData.getAvailableSeats() < paymentModel.getNoOfPassengers()){
                log.info("Booking failed. No seats left in returning flight, your payment refund has been initiated.");
                return "Booking failed. No seats left in returning flight, your payment refund has been initiated.";
            }
        }
        int availableDepartureSeats = departureFlightData.getAvailableSeats() - paymentModel.getNoOfPassengers();
        departureFlightData.setAvailableSeats(availableDepartureSeats);
        flightRepo.save(departureFlightData);

        if(paymentModel.getIsRoundTrip().equalsIgnoreCase("YES")) {
            int availableReturnSeats = returnFlightData.getAvailableSeats() - paymentModel.getNoOfPassengers();
            returnFlightData.setAvailableSeats(availableReturnSeats);
            flightRepo.save(returnFlightData);
        }
        return "Booking has been confirmed ";
    }
}
