package com.nagp.flight.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightBookingRequestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    private String source;
    private String destination;
    private int noOfPassengers;
    private String departureDate;
    private String returnDate;
    private String travellerClass;
    boolean isRoundTrip;
    private int price;
}
