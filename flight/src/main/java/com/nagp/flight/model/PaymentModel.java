package com.nagp.flight.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentModel {
    private String type;
    private String code;
    private int noOfPassengers;
    private String departureDate;
    private String returnDate;
    private String travellerClass;
    private String isRoundTrip;
    private String returnCode;
    private String checkInTime;
    private String checkOutTime;
    private int noOfRooms;
    private int noOfGuests;
}
