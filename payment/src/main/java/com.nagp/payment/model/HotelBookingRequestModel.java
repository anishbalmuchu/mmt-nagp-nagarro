package com.nagp.payment.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HotelBookingRequestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    private String city;
    private String checkInTime;
    private String checkOutTime;
    private int noOfRooms;
    private int noOfGuests;
}
