package com.nagp.payment.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Id;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HotelBookingResponseModel1 {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    private String hotelCode;
    private String hotelName;
    private String location;
    private int price;
}
