package com.nagp.payment.controller;


import com.nagp.payment.model.PaymentModel;

import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import static com.nagp.payment.constants.PaymentConstants.FETCH_PAYMENT_STATUS;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PaymentController {
@Autowired
private EurekaClient eurekaClient;
    @Autowired
    LoadBalancerClient loadBalancerClient;
    @PostMapping(value = "/payment")
    public ResponseEntity<?> payment(@RequestBody PaymentModel paymentModel) {
        RestTemplate restTemplate = new RestTemplate();
        if (paymentModel.getType().equals("HOTEL")) {
            String baseUrl = loadBalancerClient.choose("hotel").getUri().toString() + "/bookRoom";
            return restTemplate.postForEntity(baseUrl, paymentModel, String.class);
        }
        if (paymentModel.getType().equals("FLIGHT")) {
            String baseUrl = loadBalancerClient.choose("flight").getUri().toString() + "/bookFlights";
            return restTemplate.postForEntity(baseUrl, paymentModel, String.class);
        }
        return ResponseEntity.badRequest().body("Wrong TYPE of booking ID allowed - HOTEL/FLIGHT") ;
    }
}
