package com.nagp.payment.constants;

public class PaymentConstants {
    private PaymentConstants() {
        throw new IllegalArgumentException("Constants Class Instance Can't be created");}
    public static final String FETCH_PAYMENT_STATUS = "/paymentStatus";
    public static final String SEARCH_HOTEL = "/searchHotel";
}
